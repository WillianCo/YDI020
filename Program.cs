﻿using System;

namespace YDI020
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Log.LimpaArquivoLog();
                Log.GravaLog($"\n\n\n{Constants.Separator}\n** Iniciando YDI020 **", 1);
                Log.GravaLog($"** Objetivo: Obter lojas que não realizaram atualização de produtos nas últimas {Constants.Timer}, na tabela MRT.MOVPRDPCOLJASMA e enviar e-mail informativo **");
                Log.GravaLog(Constants.Separator);

                Process process = new Process();
                process.Execute();

                Log.GravaLog($"{Constants.Separator}\n** Processo YDI020 realizado com sucesso **", 1);
                Log.GravaLog(Constants.Separator);
                Log.ExitProcess(0);
            }
            catch (Exception ex)
            {
                Log.GravaLog($"{Constants.Separator}\n** Processo YDI020 finalizado com erros **", 1);
                Log.GravaLog($"{Log.GetError(ex)}\n{Constants.Separator}", 0);
                Log.ExitProcess(12);
            }
        }
    }
}
