﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace YDI020.Code.Business.Filas
{
    public class FilaBLL
    {
        /// <summary>
        /// Recuperar as lojas que não tiveram atualização de produtos em um determinado período de tempo
   
        public List<Loja> GetLojasTimer()
        {
            LojasDAL dal = new LojasDAL();
            return dal.GetLojasTimer();
        }
    }
}
