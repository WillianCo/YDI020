﻿namespace YDI020.Code.Business.Filas
{
    public class LojasDALSQL
    {
        /// <summary>
        /// Recuperar as lojas que não tiveram atualização de produtos em um determinado período de tempo

        public string GetLojasTimer()
        {
            return @"SELECT CLI.CODCLI, 
                            CLI.NOMCLI, 
                            CODEANPRDCLISMA, 
                            COUNT(*)
                     FROM MRT.CADPRDLJACTLSMA FILA 
                         INNER JOIN MRT.T0100043 CLI ON FILA.CODCLI = CLI.CODCLI
                     WHERE 1 = 1
                         AND FILA.DATDST IS NULL
                         AND FILA.CODPRD IS NOT NULL
                     GROUP BY CLI.CODCLI, CLI.NOMCLI,CODEANPRDCLISMA
                         ";
        }
    }
}
