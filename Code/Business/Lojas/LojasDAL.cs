﻿using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace YDI020.Code.Business.Filas
{
    public class LojasDAL
    {
        /// <summary>
        /// Recuperar as lojas que não tiveram atualização de produtos em um determinado período de tempo
   
        public List<Loja> GetLojasTimer()
        {
            LojasDALSQL dalsql = new LojasDALSQL();
            using (OraContext context = new OraContext())
            {
                return context.Database.SqlQuery<Loja>(dalsql.GetLojasTimer(),
                    new OracleParameter(":TIMER", Constants.Timer)).ToList();
            }
        }
    }
}