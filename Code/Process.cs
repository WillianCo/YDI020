﻿using System.Collections.Generic;
using System.Text;
using YDI020.Code.Business.Filas;

public class Process
{
    /// <summary>
    /// Executa o processo completo da aplicação
    /// </summary>

    public void Execute()
    {

        FilaBLL bll = new FilaBLL();
        //Executar validação dos clientes sambanet
        Log.GravaLog("\n");
        Log.GravaLog("*****Início consulta de produtos ativos com EANs duplicados por cliente *****", 2);
        List<Loja> lojas = bll.GetLojasTimer();
        Log.GravaLog("***** Fim consulta de clientes sem atualização de produtos *****", 2);
        Log.GravaLog("***** Início envio de e-mail de clientes sem atualização de produtos *****", 2);
        EnviarEmailProdutosDuplicados(lojas);
        Log.GravaLog("***** Fim envio de e-mail de clientes sem atualização de produtos *****", 2);
    }

    /// <summary>
    /// Enviar e-mail informativo com produtos duplicados
    /// </summary>
    /// <param name="lojas"></param>
    /// <param name="execute"></param>
  
    public void EnviarEmailProdutosDuplicados(List<Loja> lojas)
    {

        StringBuilder html = new StringBuilder();
            html.AppendLine($@"<span style=""display: block;"">Atenção, os clientes abaixo possuem produtos ativos com EANs duplicados.</span>");
            html.AppendLine($@"<span style=""display: block;margin-bottom: 30px;"">Favor desativar um dos produtos para que não haja mais de 1 produto ativo por EAN.</span>");
            html.AppendLine(@"<table border=""1"" style=""border-collapse:collapse;margin:10px 0 50px 0;width: 100%;"">");
            html.AppendLine(@"<tr style=""background-color:#f2f3f8;color:#595d6e"">");
            html.AppendLine(@"<th style=""padding:10px 5px;"">Código</th>");
            html.AppendLine(@"<th style=""padding:10px 5px;"">Cliente</th>");
            html.AppendLine(@"<th style=""padding:10px 5px;"">Código EAN</th>");
            html.AppendLine(@"</tr>");

       foreach (Loja loja in lojas)
        {
            html.AppendLine("<tr>");
            html.AppendLine($@"<td style=""padding:7px 4px;color: #595d6e;text-align: right;"">{loja.CODCLI}</td>");
            html.AppendLine($@"<td style=""padding:7px 4px;color: #595d6e;text-align: left;"">{loja.NOMCLI}</td>");
            html.AppendLine($@"<td style=""padding:7px 4px;color: #595d6e;text-align: left;"">{loja.CODEANPRDCLISMA}</td>");
            html.AppendLine("</tr>");
        }

        html.AppendLine("</table>");
        //Email.EnviaEmail(Constants.EmailDestino, "Alerta de lojas sem atualização de produtos", html.ToString());
        Email.EnviaEmail(Constants.EmailDestino, "Alerta de lojas sem atualização de produtos", html.ToString());
    }
}