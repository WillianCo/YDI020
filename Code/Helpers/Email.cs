﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

public class Email
{
    /// <summary>
    /// Envio de e-mail
    /// </summary>
    /// <param name="enderecos"></param>
    /// <param name="assunto"></param>
    /// <param name="mensagem"></param>
    
    public static void EnviaEmail(List<string> enderecos, string assunto, string mensagem)
    {
        string ServidorEmail = Constants.ServidorEmail;
        string EmailSistema = Constants.EmailAplicacao;
        string NomeEmailSistema = Constants.NomeEmailAplicacao;
        using (SmtpClient smtpCliente = new SmtpClient(ServidorEmail))
        {
            MailAddress origem = new MailAddress(EmailSistema, NomeEmailSistema);
            MailMessage email = new MailMessage();
            enderecos.ForEach(end => email.To.Add(end));
            email.From = origem;
            email.IsBodyHtml = true;
            email.Subject = assunto;
            email.Body = mensagem;
            smtpCliente.Send(email);
        }
    }

    public static void EnviaEmailDesenvolvimento(List<string> enderecos, string assunto, string mensagem)
    {
        string ServidorEmail = "smtp.gmail.com";
        string EmailSistema = Constants.EmailAplicacao;
        string NomeEmailSistema = Constants.NomeEmailAplicacao;
        using (SmtpClient smtpCliente = new SmtpClient(ServidorEmail))
        {
            MailAddress origem = new MailAddress(EmailSistema, NomeEmailSistema);
            MailMessage email = new MailMessage();
            enderecos.ForEach(end => email.To.Add(end));
            email.From = origem;
            email.IsBodyHtml = true;
            email.Subject = assunto;
            email.Body = mensagem;
            smtpCliente.EnableSsl = true; // GMail requer SSL
            smtpCliente.Port = 587;       // porta para SSL
            smtpCliente.DeliveryMethod = SmtpDeliveryMethod.Network; // modo de envio
            smtpCliente.UseDefaultCredentials = false; // vamos utilizar credencias especificas
            smtpCliente.Credentials = new System.Net.NetworkCredential("", "");
            smtpCliente.Send(email);
        }
    }
}