﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

public class Constants
{
    #region Constants

    public const string Separator = "***********************************************************************************************************************************************************";
    public const string NextExec = "...........................................................................................................................................................";

    #endregion

    #region AppSettings

    public static bool ModoDesenvolvimento
    {
        get { return !ConfigurationManager.AppSettings["ModoDesenvolvimento"].Equals("0"); }
    }

    public static string Log
    {
        get { return ConfigurationManager.AppSettings["Log"]; }
    }

    public static int Timer
    {
        get { return Convert.ToInt32(ConfigurationManager.AppSettings["Timer"]); }
    }

    #endregion    

    #region ConnectionString

    public static string ConnectionStringMRT
    {
        get
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(ConfigurationManager.AppSettings["ConfigName"]);
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("connectionStrings/add[@name='MRT001']");
                string connectionString = nodes[0].Attributes["connectionString"].Value;
                return connectionString;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Falha ao obter configuração do banco de dados. Erro: {0}", ex.Message));
            }
        }
    }

    #endregion    

    #region Email

    public static string ServidorEmail
    {
        get { return ConfigurationManager.AppSettings["ServidorEmail"]; }
    }

    public static string EmailAplicacao
    {
        get { return ConfigurationManager.AppSettings["EmailAplicacao"]; }
    }

    public static string NomeEmailAplicacao
    {
        get { return ConfigurationManager.AppSettings["NomeEmailAplicacao"]; }
    }

    public static List<string> EmailDestino
    {
        get { return ConfigurationManager.AppSettings["EmailDestino"].Split(';').ToList(); }
    }

    #endregion
}