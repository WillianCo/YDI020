﻿using System;
using System.IO;
using System.Text;

public class Log
{
    [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
    public static extern int ExitProcess(int Codigo);

    internal static void LimpaArquivoLog()
    {
        StreamWriter arq = null;
        try
        {
            DateTime lastAccess = File.GetLastWriteTime(Constants.Log);
            DateTime today = DateTime.Today;
            if (lastAccess.Date < today && (today.DayOfWeek == DayOfWeek.Thursday))
            {
                if (File.Exists(Constants.Log))
                {
                    // 'Limpa o arquivo para gravação
                    arq = new StreamWriter(Constants.Log, false);
                    arq.WriteLine(string.Empty);
                    arq.Close();
                }
            }
        }
        catch
        {
            if (arq != null)
                arq.Close();
            ExitProcess(12);
        }
    }

    internal static void GravaLog(string log, int idt = -1)
    {
        TextWriter arq = null;
        try
        {
            arq = new StreamWriter(Constants.Log, true);
            if (idt == 0)
            {
                arq.WriteLine(log);
                arq.Close();
                ExitProcess(12);
            }
            else if (idt == 1)
                arq.WriteLine(string.Format("{0} {1:dd/MM/yyyy HH:mm:ss}", log, DateTime.Now));
            else if (idt == 2)
                arq.WriteLine(string.Format("{1:HH:mm:ss} {0}", log, DateTime.Now));
            else if (idt == -1)
                arq.WriteLine(log);

            arq.Close();
        }
        catch
        {
            if (arq != null)
                arq.Close();
            ExitProcess(12);
        }
    }

    public static string GetError(Exception ex)
    {
        StringBuilder str = new StringBuilder();
        str.AppendFormat("Mensagem: {0}", ex.Message);
        if (ex.InnerException != null)
        {
            str.AppendLine("\nDetalhe Erro:");
            Exception subEx = ex.InnerException;
            while (subEx != null)
            {
                str.AppendLine(subEx.Message);
                subEx = subEx.InnerException;
            }
        }
        return str.ToString();
    }
}
