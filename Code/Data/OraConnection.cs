﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Default Oracle Connection
/// </summary>
/// Raniery Soares @PrimeTeam
public class OraConnection : IDisposable
{
    public OraConnection()
    {
        Connection = new OracleConnection(Constants.ConnectionStringMRT);
        if (Connection.State != ConnectionState.Open)
            Connection.Open();
    }

    public OraConnection(string connectionString)
    {
        Connection = new OracleConnection(connectionString);
        if (Connection.State != ConnectionState.Open)
            Connection.Open();
    }

    public OracleConnection Connection { get; set; }

    private OracleCommand command;
    public OracleCommand Command
    {
        get
        {
            if (command == null)
                command = Connection.CreateCommand();
            return command;
        }
        set
        {
            command = value;
        }
    }

    private OracleTransaction transaction;
    public OracleTransaction Transaction
    {
        get
        {
            if (transaction == null)
                transaction = Connection.BeginTransaction();
            return transaction;
        }
    }

    public void BeginTransaction()
    {
        Command.Transaction = Transaction;
    }

    public void Commit()
    {
        Transaction.Commit();
    }

    public void Rollback()
    {
        Transaction.Rollback();
    }

    public void Close()
    {
        Connection.Close();
    }

    public void Dispose()
    {
        Connection.Close();
        Connection.Dispose();
    }
}