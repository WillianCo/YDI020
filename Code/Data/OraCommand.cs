﻿using Oracle.ManagedDataAccess.Client;
using System;

/// <summary>
/// Default Oracle Command/Parameters
/// </summary>
/// Raniery Soares @PrimeTeam
public static class OraCommand
{
    /// <summary>
    /// Add simple value
    /// </summary>
    /// <param name="command"></param>
    /// <param name="parameterName"></param>
    /// <param name="parameterValue"></param>
    /// Raniery Soares @PrimeTeam
    public static void AddWithValue(this OracleCommand command, string parameterName, object parameterValue)
    {
        OracleParameter par = command.CreateParameter();
        par.ParameterName = parameterName;
        if (parameterValue == null)
            par.Value = DBNull.Value;
        else
            par.Value = parameterValue;
        command.Parameters.Add(par);
    }

    /// <summary>
    /// Add array value
    /// </summary>
    /// <param name="command"></param>
    /// <param name="parameterValue"></param>
    /// Raniery Soares @PrimeTeam
    public static void AddWithArray(this OracleCommand command, object parameterValue)
    {
        OracleParameter par = command.CreateParameter();
        if (parameterValue == null)
            par.Value = DBNull.Value;
        else
            par.Value = parameterValue;
        command.Parameters.Add(par);
    }

    /// <summary>
    /// Convert primitive type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="input"></param>
    /// <returns></returns>
    /// Raniery Soares @PrimeTeam
    public static T ConvertPrimaryValue<T>(object input)
    {
        if (input == DBNull.Value)
            return default(T);
        else
            return (T)Convert.ChangeType(input, typeof(T));
    }

    /// <summary>
    /// Convert primitive nullable type 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Y"></typeparam>
    /// <param name="input"></param>
    /// <returns></returns>
    /// Raniery Soares @PrimeTeam
    public static dynamic ConvertPrimaryValue<T, Y>(object input)
    {
        if (input == DBNull.Value)
            return default(Y);
        else
            return (T)Convert.ChangeType(input, typeof(T));
    }

    /// <summary>
    /// Convert complex type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="input"></param>
    /// <returns></returns>
    /// Raniery Soares @PrimeTeam
    public static T ConvertComplexValue<T>(object input)
        where T : class
    {
        if (input == DBNull.Value)
            return null;
        else
            return (T)Convert.ChangeType(input, typeof(T));
    }
}