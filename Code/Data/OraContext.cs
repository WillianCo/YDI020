﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

/// <summary>
/// Mapped Database Context
/// </summary>
/// Raniery @Prime Team
public class OraContext : DbContext
{
    public OraContext() : base(new OracleConnection(Constants.ConnectionStringMRT), true) { }

    public OraContext(string connectionString) : base(new OracleConnection(connectionString), true) { }

    public OraContext(OracleConnection connection) : base(connection, false) { }
}